import React from 'react';
import ReactDOM from 'react-dom/client';

import { App } from './App';
import { BrowserRouter } from 'react-router-dom';
import { CountProvider } from './Context/countContext';
import { ColorSwitchProvider } from './Context/colorSwitchContext';

const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);


root.render(
  <ColorSwitchProvider>
    <CountProvider>
      <BrowserRouter>
        <App />
      </BrowserRouter>
    </CountProvider>
  </ColorSwitchProvider>
);

