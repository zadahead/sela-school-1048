import './Line.css';

export const Line = (props: any) => {
    return (
        <div className="Line">
            {props.children}
        </div>
    )
}