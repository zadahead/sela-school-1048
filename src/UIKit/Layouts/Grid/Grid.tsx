import './Grid.css';

export const Grid = (props: any) => {
    return (
        <div className='Grid'>
            {props.children}
        </div>
    )
}