import { Component, ReactNode } from 'react';
import './Between.css';

export const Between = (props: any) => {
    return (
        <div className="Between">
            {props.children}
        </div>
    )
}