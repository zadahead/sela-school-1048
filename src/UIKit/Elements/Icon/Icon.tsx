import './Icon.css';

export const Icon = ({ i }: IIconProps) => {
    return (
        <span className="Icon material-symbols-outlined">{i}</span>
    )
}

export type Icons = 'search' | 'favorite' | 'star'

interface IIconProps {
    i: Icons;
}