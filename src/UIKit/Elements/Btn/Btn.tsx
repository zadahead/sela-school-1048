import { forwardRef } from 'react';
import { Line } from '../../Layouts/Line/Line';
import { Icon, Icons } from '../Icon/Icon';
import './Btn.css';

export const Btn = forwardRef((props: IBtn, ref: React.ForwardedRef<any>) => {

    const { children, onClick, i } = props;

    return (
        <button ref={ref} className='Btn' onClick={onClick}>
            <Line>
                {i && <Icon i={i} />}
                {children}
            </Line>
        </button>
    )
})

interface IBtn {
    children: string;
    onClick: (e: any) => void;
    i?: Icons;
}