import { createContext } from "react";
import { useColorSwitch } from "../Hooks/useColorSwitch";

export const colorSwitchContext = createContext<IColorSwitchContext>({
    color: '',
    handleSwitch: () => { }
});

const Provider = colorSwitchContext.Provider;

export const ColorSwitchProvider = ({ children }: IColorSwitchProvider) => {
    const { color, handleSwitch } = useColorSwitch();

    const value = {
        color,
        handleSwitch
    }

    return (
        <Provider value={value}>
            {children}
        </Provider>
    )
}

interface IColorSwitchProvider {
    children: JSX.Element[] | JSX.Element | string;
}

export interface IColorSwitchContext {
    color: string;
    handleSwitch: () => void;
}