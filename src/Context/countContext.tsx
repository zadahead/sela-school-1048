import { createContext, useState } from 'react';

export const countContext = createContext<ICountContext>({
    count: 0,
    setCount: () => { },
    handleAdd: () => { }
});

const Provider = countContext.Provider;


export const CountProvider = ({ children }: ICountProvider) => {
    const [count, setCount] = useState(0);

    const handleAdd = () => {
        setCount(count + 1);
    }


    const value = {
        count,
        setCount,
        handleAdd,
        name: 'mosh'
    }

    return (
        <Provider value={value}>
            {children}
        </Provider>
    )

}


interface ICountProvider {
    children: JSX.Element[] | JSX.Element | string;
}

export interface ICountContext {
    count: number;
    setCount: React.Dispatch<React.SetStateAction<number>>;
    handleAdd: () => void;
}