import { Box } from "./Components/Box"
import { Welcome } from "./Components/Welcome"
import { Line } from "./UIKit/Layouts/Line/Line"
import { useContext } from 'react';

import './App.css';
import { Between } from "./UIKit/Layouts/Between/Between";
import { Grid } from "./UIKit/Layouts/Grid/Grid";
import { Icon } from "./UIKit/Elements/Icon/Icon";
import { Btn } from "./UIKit/Elements/Btn/Btn";
import { Link, NavLink, Route, Routes } from "react-router-dom";
import { AboutView } from "./Views/AboutView";
import { HomeView } from "./Views/HomeView";
import { StateView } from "./Views/StateView";
import { ListView } from "./Views/ListView";
import { LifeCycleView } from "./Views/LifeCycleView";
import { TodosView, UsersView } from "./Views/TodosView";
import { RefView } from "./Views/RefView";
import { HooksView } from "./Views/HooksView";
import { ContextView } from "./Views/ContextView";
import { countContext } from "./Context/countContext";
import { colorSwitchContext } from "./Context/colorSwitchContext";

export const App = () => {
    const { count } = useContext(countContext);
    const { color } = useContext(colorSwitchContext);

    const styleCss = {
        backgroundColor: color
    }

    return (
        <div className="App">
            <Grid>
                <header>
                    <Between>
                        <Line>
                            <Icon i="star" />
                            <h3 style={styleCss}>{count}</h3>
                        </Line>


                        <Line>
                            <NavLink to="/about">About</NavLink>
                            <NavLink to="/state">State</NavLink>
                            <NavLink to="/list">List</NavLink>
                            <NavLink to="/cycle">Cycle</NavLink>
                            <NavLink to="/todos">Todos</NavLink>
                            <NavLink to="/ref">Ref</NavLink>
                            <NavLink to="/hooks">Hooks</NavLink>
                            <NavLink to="/context">Context</NavLink>
                        </Line>
                    </Between>
                </header>
                <main>
                    <div>
                        <Routes>
                            <Route path="/about" element={<AboutView />} />
                            <Route path="/home" element={<HomeView />} />
                            <Route path="/state" element={<StateView />} />
                            <Route path="/list" element={<ListView />} />
                            <Route path="/cycle" element={<LifeCycleView />} />
                            <Route path="/todos" element={(
                                <>
                                    <UsersView />
                                    <TodosView />
                                </>
                            )} />
                            <Route path="/ref" element={(
                                <>
                                    <RefView />
                                    <RefView />
                                </>
                            )} />
                            <Route path="/hooks" element={<HooksView />} />
                            <Route path="/context" element={(
                                <>
                                    <ContextView />
                                    <ContextView />
                                </>
                            )} />
                        </Routes>
                    </div>
                </main>
                <footer></footer>
            </Grid>


            {/*             

            <Box title="Another box" >
                <h3>asdassssd</h3>
                <h3>elem 3</h3>
            </Box>

            <Welcome source="welcome" /> */}
        </div>
    )
}