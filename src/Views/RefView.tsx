import { useState, useEffect, useRef } from 'react';
import { Btn } from '../UIKit/Elements/Btn/Btn';



export const RefView = () => {
    const [count, setCount] = useState(0);
    const countRef = useRef(0);
    const h1Ref = useRef<HTMLHeadingElement>(null);

    countRef.current = count;

    useEffect(() => {
        setTimeout(() => {
            if (h1Ref.current && countRef.current > 5) {
                h1Ref.current.style.backgroundColor = 'red';
            }

            document.getElementById('myh1')!.style.backgroundColor = 'yellow';


            setCount(countRef.current + 55);
        }, 2000)
    }, [])


    return (
        <div>

            <h2 id="myh1" >RefView, {count}</h2>
            <Btn ref={h1Ref} onClick={() => setCount(count + 1)}>Add</Btn>
        </div>
    )
}