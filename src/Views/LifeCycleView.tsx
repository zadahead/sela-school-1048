import { useEffect, useState } from 'react';
import { Counter } from '../Components/Counter';
import { Btn } from '../UIKit/Elements/Btn/Btn';
import axios from 'axios';

export const LifeCycleView = () => {
    const [isLoading, setIsLoading] = useState(true);

    const [count, setCount] = useState(+localStorage.getItem('COUNT')!);

    console.log('render');

    useEffect(() => {
        fetch('https://jsonplaceholder.typicode.com/todos')
            .then(response => response.json())
            .then(json => console.log(json))

        axios.get('https://jsonplaceholder.typicode.com/todos')
            .then(resp => {
                console.log(resp.data);
            })
    }, [])

    useEffect(() => {
        console.log('mounted');
        document.body.addEventListener('click', handleBodyClick);

        setTimeout(() => {
            setIsLoading(false);
            setCount(count + 1);
        }, 2000)

        return () => {
            console.log('will unmount');
            document.body.removeEventListener('click', handleBodyClick);
        }

    }, [])



    useEffect(() => {
        console.log('count', count);
        localStorage.setItem('COUNT', count.toString());

        return () => {
            console.log('count will', count);
        }
    }, [count])

    const handleBodyClick = () => {
        console.log('handleBodyClick');
    }



    return (
        <div>
            <h1>Count, {count}</h1>
            {isLoading ? <h1>loading...</h1> : <h1>loaded</h1>}
            <Btn onClick={() => setCount(count + 1)}>Add</Btn>
        </div>
    )
}