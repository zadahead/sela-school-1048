import { memo } from 'react';
import { useCounter } from '../Hooks/useCounter';
import { Btn } from '../UIKit/Elements/Btn/Btn';
import { Line } from '../UIKit/Layouts/Line/Line';


export const AboutView = () => {
    const { count, handleAdd } = useCounter();
    const { count: count2, handleAdd: handleAdd2 } = useCounter();


    console.log('<AboutView />');

    return (
        <div>
            <h1>About View, {count} -- {count2}</h1>
            <Line>
                <Btn onClick={handleAdd}>Add</Btn>
                <Btn onClick={handleAdd2}>Add2</Btn>
            </Line>
            <Inner count={count} handleAdd={handleAdd} />
        </div>
    )
}


const Inner = memo(({ count, handleAdd }: any) => {
    console.log('<Inner />');

    return (
        <div>
            <h2>Inner, {count}</h2>
            <Btn onClick={handleAdd}>Add Inner</Btn>
            <ExtraInner />
        </div>
    )
})

const ExtraInner = () => {
    console.log('<ExtraInner />');

    return (
        <div>
            <h3>Extra Inner</h3>
        </div>
    )
}
