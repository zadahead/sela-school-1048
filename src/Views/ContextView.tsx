import { useContext } from 'react';
import { countContext } from '../Context/countContext';
import { Btn } from '../UIKit/Elements/Btn/Btn';
import { useColorSwitch } from '../Hooks/useColorSwitch';
import { Line } from '../UIKit/Layouts/Line/Line';
import { colorSwitchContext } from '../Context/colorSwitchContext';

export const ContextView = () => {
    const { count, handleAdd } = useContext(countContext);
    const { color, handleSwitch } = useContext(colorSwitchContext);

    const styleCss = {
        color
    }

    return (
        <div>
            <h2 style={styleCss}>Context View, {count}</h2>
            <Line>
                <Btn onClick={handleAdd}>Add</Btn>
                <Btn onClick={handleSwitch}>Switch</Btn>
            </Line>
        </div>
    )
}