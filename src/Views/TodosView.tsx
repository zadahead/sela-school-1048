import { useFetch } from "../Hooks/useFetch";

interface IFetch {
    url: string;
    field: string;
}

const Fetch = ({ url, field }: IFetch) => {
    const { list, isLoading, error } = useFetch(url);


    //render
    const renderList = () => {
        if (error) {
            return <h3>{error}</h3>
        }
        if (isLoading) {
            return <h3>Loading..</h3>
        }

        console.log(list);

        return list.map((i: any) => {
            return <h3 key={i.id}>{i[field]}</h3>
        })
    }

    return renderList();
}


export const TodosView = () => {

    return (
        <div>
            <h1>Todos</h1>
            <Fetch url="/todos" field="title" />
        </div>
    )
}

export const UsersView = () => {

    return (
        <div>
            <h1>Users</h1>
            <Fetch url="/users" field="email" />
        </div>
    )
}