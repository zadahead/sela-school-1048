
import { useState } from 'react';
import { Btn } from '../UIKit/Elements/Btn/Btn';

const users = [
    {
        id: crypto.randomUUID(),
        name: 'Mosh',
        complete: false,
        address: {
            city: 'haifa'
        }
    },
    {
        id: crypto.randomUUID(),
        name: 'David',
        complete: false,
        address: {
            city: 'tlv'
        }
    },
    {
        id: crypto.randomUUID(),
        name: 'Ruth',
        complete: true,
        address: {
            city: 'jer'
        }
    }
]

export const ListView = () => {
    const [list, setList] = useState(users);

    const handleAdd = () => {
        list.push({
            id: crypto.randomUUID(),
            name: 'sdsd',
            complete: false,
            address: {
                city: 'dd'
            }
        })

        console.log(list);
        setList([...list]);
    }

    const handleToggle = (item: any) => {
        console.log('handleToggle', item)
        item.complete = !item.complete;
        item.address.city += 'a';

        setList([...list]);
    }

    const renderList = () => {
        return list.map((u, index) => {
            const styleCss = {
                color: u.complete ? '#b1b1b1' : '#333'
            }
            return (
                <h3
                    onClick={() => handleToggle(u)}
                    style={styleCss}
                    key={u.id}
                >
                    {`${u.name} --- ${u.address.city}`}
                </h3>
            )
        })
    }


    return (
        <div>
            <h1>List View</h1>
            <Btn onClick={handleAdd}>Add</Btn>
            {renderList()}
        </div>
    )
}