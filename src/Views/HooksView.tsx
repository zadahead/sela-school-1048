import { Btn } from '../UIKit/Elements/Btn/Btn';
import { useColorSwitch } from '../Hooks/useColorSwitch';
import { useCounter } from '../Hooks/useCounter';
import { Line } from '../UIKit/Layouts/Line/Line';

/*
    1) Create a fully working component
    2) separate the logic from the render
    3) create a function starting with "use" and paste all the logic in
    4) return all the values the render needs from the "use" function
    5) call the "use" function from the main component
    6) export the "use" funtion from its own file. 
*/




export const HooksView = () => {
    //logic
    const { color, handleSwitch } = useColorSwitch();
    const { count, handleAdd } = useCounter();

    //render    

    const styleCss = {
        backgroundColor: color
    }


    return (
        <div>
            <h2 style={styleCss}>Hooks View, {count}</h2>
            <Line>
                <Btn onClick={handleSwitch}>Switch</Btn>
                <Btn onClick={handleAdd}>Add</Btn>
            </Line>
        </div>
    )
}