import { useState } from "react";
import { ColorSwitch } from "../Components/ColorSwitch"
import { Counter } from "../Components/Counter"
import { Btn } from "../UIKit/Elements/Btn/Btn";
import { Welcome } from "../Components/Welcome";
import { Toggler } from "../Components/Toggler";

export const StateView = () => {
    const [value, setValue] = useState('');

    const handleChange = (e: any) => {
        console.log(e.target.value);
        setValue(e.target.value + 'ff');
    }

    const handleSubmit = () => {
        console.log(value);
    }


    return (
        <div>
            <h1>State View</h1>

            {/* <input onChange={handleChange} />

            <Btn onClick={handleSubmit}>Submit</Btn>

            <ColorSwitch />

            <ColorSwitch /> */}

            <Toggler>
                <ColorSwitch />
            </Toggler>
        </div>
    )
}