import { Btn } from "../UIKit/Elements/Btn/Btn"
import { Line } from "../UIKit/Layouts/Line/Line"

export const HomeView = () => {
    const handleClick = () => {
        console.log('handleClick');
    }

    return (
        <div>
            <Line>
                <button onClick={handleClick}>Click me</button>
                <Btn i="favorite" onClick={handleClick}>clickkkk</Btn>
                <Btn onClick={handleClick}>clickkkk</Btn>
            </Line>
        </div>
    )
}