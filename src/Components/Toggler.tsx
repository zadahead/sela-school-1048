import { useState } from 'react';
import { Btn } from "../UIKit/Elements/Btn/Btn"

export const Toggler = ({ children }: IProps) => {

    const [isDisp, setIsDisp] = useState(true);

    const handleToggle = () => {
        setIsDisp(!isDisp);
    }

    return (
        <div>
            <h1>Toggler</h1>
            <Btn onClick={handleToggle}>Toggle</Btn>

            {isDisp && (
                <div>
                    {children}
                </div>
            )}
        </div>
    )
}

interface IProps {
    children: JSX.Element | JSX.Element[] | string
}