import { useState } from "react"
import { Btn } from "../UIKit/Elements/Btn/Btn"

export const Counter = ({ count, setCount, jump = 1} : any) => {
    console.log('render');

    const handleAdd = () => {
        setCount(count + jump);
    }

    console.log("count", count);

    const styleCss = {
        backgroundColor: 'red',
        color: "#fff"
    }

    return (
        <div>
            <h3 style={styleCss}>Count, {count}</h3>
            <Btn onClick={handleAdd}>Add</Btn>
        </div>
    )
}