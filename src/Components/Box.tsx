export const Box = (props: IBoxProps) => {
    const { title, children } = props;

    const wrapCss = {
        backgroundColor: 'aqua',
        padding: '20px',
        margin: '10px'
    }

    const contentCss = {
        backgroundColor: '#e1e1e1',
        padding: '10px'
    }

    return (
        <div style={wrapCss}>
            <h1>{title}</h1>
            <div style={contentCss}>
                {children}
            </div>
        </div>
    )
}

interface IBoxProps {
    title: string;
    children: JSX.Element | JSX.Element[] | string
}