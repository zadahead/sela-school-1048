import { useState } from "react"
import { Btn } from "../UIKit/Elements/Btn/Btn"


export const ColorSwitch = () => {
    console.log('render');
    const [color, setColor] = useState('red'); //['red', () => {}]
    const [count, setCount] = useState(0); //1

    const handleSwitch = () => {
        console.log('handleSwitch')
        setColor(color === 'red' ? 'blue' : 'red');
        if (color === 'red') {
            setCount(count + 1);
        }
    }

    const styleCss = {
        backgroundColor: color,
        color: '#fff'
    }

    return (
        <div>
            <h2 style={styleCss}>Color {count}</h2>
            <Btn onClick={handleSwitch}>Switch</Btn>
        </div>
    )
}