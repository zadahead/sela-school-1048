
export const User = ({ name, source, age }: IUserProps) => {

    return (
        <h2>User ({source}) {name} age: {age}</h2>
    )
}



interface IUserProps {
    name: string;
    age: number;
    source: string;
}