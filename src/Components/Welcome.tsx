import { User } from "./User";

export const Welcome = (props: any) => {
    console.log(props);

    const { source } = props;

    return (
        <div>
            {props.children}
            <h1 style={props.style}>Welcome!</h1>

            {!props.hide && (
                <>
                    <User name="Mosh" age={34} source={source} />
                    <User name='David' age={66} source={source} />
                </>
            )}
        </div>
    )
}
