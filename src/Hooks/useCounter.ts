import { useState, useCallback } from 'react';


export const useCounter = () => {

    const [count, setCount] = useState(() => 5);

    const handleAdd = useCallback(() => {
        setCount((currentState) => {
            return currentState + 1;
        })
    }, []);

    return {
        count,
        handleAdd
    }
}