import { useState, useEffect } from 'react';
import axios from 'axios';

export const useFetch = (url: string) => {
    const [isLoading, setIsLoading] = useState(true); //false
    const [list, setList] = useState<any>([]); //[ .... ]
    const [error, setError] = useState('');

    useEffect(() => {
        setTimeout(() => {
            axios.get('https://jsonplaceholder.typicode.com' + url)
                .then(resp => {
                    setList(resp.data);
                    setIsLoading(false);
                })
                .catch((err: any) => {
                    setError(err.message);
                })
        }, 1500)
    }, [])

    return {
        isLoading,
        list,
        error
    }
}